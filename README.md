# TEMPLEOS ELEPHANT WALLPAPERS

Made wallpapers of the TempleOS elephant both in gif and png formats.

## List of Sizes

**Desktop:**

640x480, 800x600, 1024x768, 1280x800, 1280x1024, 1366x768, 1440x900, 1600x900,1920x550, 1920x1080, 1920x1200, 2304x1440, 2560x1080, 2560x1440, 2560x1600, 2560x1664, 2560x2880, 2880x1800, 3024x1964, 3440x1440, 3456x2234, 3840x1080, 3840x1600, 3840x2160, 4480x2520, 5120x1440, 5120x2160, 5120x2880, 6016x3384

**Mobile:**

240x320, 720x1600, 1080x1920, 1080x2304, 1080x2340, 1080x2400, 1080x2448, 1080x2520, 1080x2640, 1170x2532, 1179x2556, 1240x2772, 1284x2778, 1290x2796, 1440x3088, 1440x3120, 1440x3168, 1440x3216, 1644x3840, 1840x2208

## Software for Live Wallpaper

This some recommended software, but there is other software you can use.

**Windows:**

Wallpaper Engine

**Android:**

GIF Live Wallpaper by Redwarp

**Linux:**

mpv, wallset